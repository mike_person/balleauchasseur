﻿using Microsoft.AspNetCore.SignalR;
using Optimiz.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Optimiz.Hubs
{
    public class TrackHub : Hub
    {

        public async Task TrackMe(Gibier Bambi)
        { 
            await Clients.AllExcept(Context.ConnectionId).SendAsync("UpdateTrack", Bambi);
        }
    }
}
